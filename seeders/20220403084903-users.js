'use strict';

const users = [
    {
        firstName: "John",
        lastName: "Doe",
        age: 23,
        createdAt: new Date(),
        updatedAt: new Date()
    }, {
        firstName: "Susi",
        lastName: "Sue",
        age: 27,
        createdAt: new Date(),
        updatedAt: new Date()
    }, {
        firstName: "Wombo",
        lastName: "Combo",
        age: 999,
        createdAt: new Date(),
        updatedAt: new Date()
    }
]

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.bulkInsert('Users', users)
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.bulkDelete('Users', null, {});
    }
};
