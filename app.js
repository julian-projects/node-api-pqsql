const express = require('express')
const bodyParser = require('body-parser')
const db = require('./models')

const app = express()
const port = 3030

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

app.set("db", db);

app.get('/', (request, response) => {
    response.json({info: 'Node.js, Express, and Postgres API'})
})

require('./routes')(app);

app.listen(port, () => {
    console.log(`App running on port ${port}.`)
})
