const express = require('express');
const router = express.Router();

const users = require('../backend/users');

router.get('/', async (req, res) => {
    const db = req.app.get('db');

    res.json({"Endpoint": "Users"});
})

router.get('/:id', async (req, res) => {
    const db = req.app.get('db');
    const id = req.params.id;

    try {
        const user = await users.findOne(db, id);

        res.json(user);
    } catch (e) {}
})

module.exports = router;
