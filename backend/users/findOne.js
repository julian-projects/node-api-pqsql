const findOne = async(db, data) => {
    const Model = db.User;

    const user = await Model.findOne({
        where: {
            id: data
        }
    });

    return user.toJSON();
}

module.exports = findOne;
