const findOne = require('./findOne.js');
const update = require('./update.js');

module.exports = {
    findOne,
    update,
}