# Postgres
## Installation
### Mac
```bash
brew install postgresql
```
After installation start service
```bash
brew services start postgresql
==> Successfully started `postgresql` (label: homebrew.mxcl.postgresql)
```
If at any point you want to stop the postgresql service, you can `run brew services stop postgresql`.
### Windows
[Guide](https://www.postgresql.org/download/windows/)
### Linux
[Guide](https://www.postgresqltutorial.com/postgresql-getting-started/install-postgresql-linux/)
## Setup
Useful comands
- \q | Exit psql connection
- \c | Connect to a new database
- \dt | List all tables
- \du | List all roles
- \list | List databases

Connect to default database
```bash
psql postgres
```
Show users
```bash
postgres=# \du
```
Create Role
```bash
postgres=# CREATE ROLE studytalkuser WITH LOGIN PASSWORD 'studytalk';
```
Change permission
```bash
postgres=# ALTER ROLE studytalkuser CREATEDB;
```
Quit and login with new user
```bash
postgres=# \q
```
```bash
psql -d postgres -U studytalkuser
```

Create Database
```bash
postgres=> CREATE DATABASE studytalk;
```
use `\list` to show Databases

Connect to Database
```bash
postgres=> \c studytalk
You are now connected to database "studytalk" as user "studytalkuser".
api=>
```
Show Table content
```bash
SELECT * FROM "Users";
```
# Project
## Prerequisites
- Postgres Database
## 1. Getting started
```bash
npm install
```
## 2. Initialize Database
```bash
npm run init
```
Check if users are created


## 3. Start Server
```bash
npm run dev
```
## Check
- http://localhost:3030/
- http://localhost:3030/api/users/2

# Knowlege
## Config
A `config/config.json` file that will contain the necessary configuration to connect to our database in development, staging, and production environments.

## Migrations
A `migrations/` directory. Migrations are scripts that allow us to reliably transform with time our database schema and keep it consistent across environments. In other words, if we ever change our mind about the database schema we designed, migrations are the best way to change it without sweating on losing our data.

## Seeders
A `seeders/` directory. Seeders are scripts to inject data in our database. We will use that to populate our database tables with test data.

## Models
A `models/` directory which will have models. Models are simply blueprinting functions that map directly to our database tables. We will have a model for every table in our schema.

# Sources
- [Knowlege](https://tinloof.com/blog/how-to-create-manage-a-postgres-database-in-node-js-from-scratch-tutorial)
- [Postgres](https://blog.logrocket.com/nodejs-expressjs-postgresql-crud-rest-api-example/)
